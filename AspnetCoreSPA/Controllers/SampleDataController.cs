using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CsvHelper;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;

namespace AspnetCoreSPATemplate.Controllers
{
    [Route("api/[controller]")]
    public class SampleDataController : Controller
    {

        [HttpPost("[action]")]

        public ResponseResult GetUsersByKeyword([FromBody]Filter dtFilter)
        {
            List<Users> results = new List<Users>();
            Users oneItem = new Users();
            int pagesize = 20;

            TextReader reader = new StreamReader("SampleData.csv");
            var csvReader = new CsvReader(reader);
            results = csvReader.GetRecords<Users>().ToList();
            var allItem = new List<Users>();
            if (string.IsNullOrEmpty(dtFilter.keyword)||string.IsNullOrWhiteSpace(dtFilter.keyword))
            {
               
                allItem = results.ToList();

            }
            else
            {
                allItem = results.Where(Item =>
                             Item.first_name.Trim().ToLower().Contains(dtFilter.keyword.Trim().ToLower()) ||
                             Item.last_name.Trim().ToLower().Contains(dtFilter.keyword.Trim().ToLower()) ||
                             Item.email.Trim().ToLower().Contains(dtFilter.keyword.Trim().ToLower()) ||
                             Item.phone1.Trim().ToLower().Contains(dtFilter.keyword.Trim().ToLower())
                            ).ToList();
            }



            ResponseResult finalResponse = new ResponseResult()
            {
                pageModel = new ModelPageResponse()
                {
                    PageIndex = dtFilter.currentpage,
                    TotalItem = allItem.Count(),
                    PageSize = pagesize
                },
                Result = allItem.Skip((dtFilter.currentpage - 1) * pagesize).Take(pagesize).ToList()

            };
            return finalResponse;


        }

    }
}

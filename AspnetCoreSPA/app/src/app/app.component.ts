
import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {HttpClient} from '@angular/common/http'
import {AppService} from './app.services'
import {User} from './Models/user.model'
import { UserDataSource } from './Models/user-datasource.model';
import { tap, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import {merge, fromEvent} from 'rxjs';
import { MatSort } from '@angular/material/sort';
 
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  dataSource =null;
  totalPage:Number=0;
  pageIndex:Number=0;
  pageSize:Number=20;
  keyword:string ="";
  
  private ELEMENT_DATA: User[] = [];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild('input', {static: true}) input: ElementRef;
  constructor (private appService:AppService){

  }
  ngOnInit() {
    this.dataSource = new UserDataSource(this.appService);
    this.dataSource.loadUsers({keyword:"",currentpage:1});
  }
  ngAfterViewInit() {

    fromEvent(this.input.nativeElement,'keyup')
    .pipe(
        debounceTime(150),
        distinctUntilChanged(),
        tap(() => {
            this.paginator.pageIndex = 0;
            this.LoadUser();
        })
    )
    .subscribe();


    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

        // on sort or paginate events, load a new page
        merge(this.sort.sortChange, this.paginator.page)
        .pipe(
            tap(() => this.LoadUser())
        )
        .subscribe();
}
LoadUser(){
    
    var filter={
      keyword:this.input.nativeElement.value,
      currentpage:this.paginator.pageIndex+1
    }
    this.dataSource = new UserDataSource(this.appService);
    this.dataSource.loadUsers(filter);  
  }
}



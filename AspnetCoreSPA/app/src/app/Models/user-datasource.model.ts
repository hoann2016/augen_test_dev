import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import { User } from './user.model';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { AppService } from '../app.services';
import { finalize, catchError } from 'rxjs/operators';

export class UserDataSource implements DataSource<User> {

    private usersSubject = new BehaviorSubject<User[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    public totalItem =new BehaviorSubject<number>(null);

    public loading$ = this.loadingSubject.asObservable();

    constructor(private appService: AppService) {}

    connect(collectionViewer: CollectionViewer): Observable<User[]> {
        return this.usersSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.usersSubject.complete();
        this.loadingSubject.complete();
        this.totalItem.complete();
    }
  
    loadUsers(filter:any) {
        console.log(filter);
        this.loadingSubject.next(true);
        this.appService.getUser(filter).pipe(
            catchError(() => of([])),
            finalize(() => this.loadingSubject.next(false))
        )
        .subscribe(listUsers =>{
            var res:User[]=[];
            this.totalItem.next(listUsers.pageModel.totalItem);
            listUsers.result.forEach(element => {
                    res.push({
                    FirstName:element.first_name,
                    LastName:element.last_name,
                    Email:element.email,
                    Phone:element.phone1,
                    });
                
                })
                this.usersSubject.next(res);
            } );

    }  
}
import { User } from "./user.model";

export interface UserResponse{
    totalPage:number;
    pageIndex:number;
    pageSize:number;
    result:User[];
}
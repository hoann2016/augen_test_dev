import { Injectable } from '@angular/core';
import {
  HttpClient,
  
} from '@angular/common/http';
import { Observable, of, Subject, interval } from 'rxjs';
import { map, tap } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class AppService {
  private urlGetAllUser = '/api/sampledata/GetUsersByKeyword';
  constructor(private http: HttpClient) { }
  getUser(filter:any): Observable<any> {
    return this.http.post<any>(this.urlGetAllUser,filter).pipe(
      map((res: any) => {
        return res;
      })
    );
  }

}

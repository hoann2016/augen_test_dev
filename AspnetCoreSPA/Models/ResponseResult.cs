using System.Collections.Generic;

public class ResponseResult {
    public ModelPageResponse pageModel{get;set;}
    public List<Users> Result{get;set;}
}

public class ModelPageResponse
{
    public int TotalItem{get;set;}
    public int PageIndex{get;set;}
    public int PageSize{get;set;}
}